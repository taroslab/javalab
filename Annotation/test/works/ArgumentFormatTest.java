package works;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

class ArgumentFormatTest {

	@Test
	void test() {
		String day = getDay("2018/09/11");

		assertThat(day, is("11"));
	}

	String getDay(@ArgumentFormat("^[0-9]{4}/[0-9]{2}/[0-9]{2}$") String date) {
		return date.substring(8);
	}

}
