package stream;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Streamの作成
 *
 * @author Taro
 */
public class CreateStream {

	/**
	 * ListからStreamを作成する。
	 *
	 * @param list リスト
	 * @return ストリーム
	 */
	public <T> Stream<T> createStream(List<T> list) {
		return list.stream();
	}

	/**
	 * SetからStreamを作成する。
	 *
	 * @param set セット
	 * @return ストリーム
	 */
	public <T> Stream<T> createStream(Set<T> set) {
		return set.stream();
	}

	/**
	 * Mapからストリームを作成する。
	 *
	 * @param map マップ
	 * @return ストリーム
	 */
	public <K, V> Stream<Entry<K, V>> createStream(Map<K, V> map) {
		return map.entrySet().stream();
	}

	/**
	 * 任意の要素からStreamを作成する。
	 *
	 * @param values 可変長引数
	 * @return ストリーム
	 */
	@SafeVarargs
	public final <T> Stream<T> createStream(T... values) {
		return Stream.of(values);
	}

	/**
	 * 配列からStreamを作成する。
	 *
	 * @param array 配列
	 * @return ストリーム
	 */
	public <T> Stream<T> createArrayStream(T[] array) {
		return Arrays.stream(array);
	}

	/**
	 * Integer型のStreamを作成する。開始(startInclusive)と終了(endExclusive)を指定する。
	 * ただし、末尾を含まない(startInclusive ～ endExclusive - 1 のStreamが作成される)。
	 *
	 * @param startInclusive 開始
	 * @param endExclusive 終了
	 * @return Integer型のストリーム
	 */
	public <T> IntStream createIntStream(int startInclusive, int endExclusive) {
		return IntStream.range(startInclusive, endExclusive);
	}

	/**
	 * Integer型のStreamを作成する。開始(startInclusive)と終了(endInclusive)を指定する。
	 * ただし、末尾を含む(startInclusive ～ endInclusive のStreamが作成される)。
	 *
	 * @param startInclusive 開始
	 * @param endInclusive 終了
	 * @return Integer型のストリーム
	 */
	public <T> IntStream createIntStreamClosed(int startInclusive, int endInclusive) {
		return IntStream.rangeClosed(startInclusive, endInclusive);
	}

	/**
	 * Double型のStreamを作成する。
	 *
	 * @param values Double型の可変長引数
	 * @return Double型のストリーム
	 */
	public <T> DoubleStream createDoubleStream(double... values) {
		return DoubleStream.of(values);
	}
}
