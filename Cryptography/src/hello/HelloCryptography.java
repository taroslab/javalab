package hello;

/**
 * Hello Cryptographyを表示するクラス
 *
 * @author Taro
 */
public class HelloCryptography {

	/**
	 * Hello Cryptography処理メイン。
	 */
	public void helloCryptographyMain() {
		System.out.println("Hello Cryptography");
	}
}
