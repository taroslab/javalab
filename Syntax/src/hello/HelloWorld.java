package hello;

/**
 * Hello Worldを表示するクラス
 *
 * @author Taro
 */
public class HelloWorld {

	/**
	 * Hello World処理メイン。
	 *
	 * @param args コマンドライン引数
	 */
	public static void main(String... args) {
		System.out.println("Hello World");
	}
}
