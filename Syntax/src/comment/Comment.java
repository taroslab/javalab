package comment;

/**
 * コメントの記述
 *
 * @author Taro
 */
public class Comment {

	// これは1行コメント。行末までコメントとなる。

	/*
	  これは複数行コメント。
	*/
}
