package operator;

/**
 * 算術演算子(Arithmetic operator)
 *
 * @author Taro
 */
public class ArithmeticOperator {

	/**
	 * 加算する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 和(summation)
	 */
	public double add(double leftOpe, double rightOpe) {
		return leftOpe + rightOpe;
	}

	/**
	 * 減算する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 差(difference)
	 */
	public double subtract(double leftOpe, double rightOpe) {
		return leftOpe - rightOpe;
	}

	/**
	 * 乗算する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 積(product)
	 */
	public double multiply(double leftOpe, double rightOpe) {
		return leftOpe * rightOpe;
	}

	/**
	 * 除算する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 商(quotient)
	 * @throws ArithmeticException 0で除算した場合。
	 */
	public double divide(double leftOpe, double rightOpe) throws ArithmeticException {
		return leftOpe / rightOpe;
	}

	/**
	 * 整数で除算する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 商の整数部(integer part)
	 * @throws ArithmeticException 0で除算した場合。
	 */
	public int divide(int leftOpe, int rightOpe) throws ArithmeticException {
		return leftOpe / rightOpe;
	}

	/**
	 * 剰余算する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 剰余(remainder)
	 * @throws ArithmeticException 0で除算した場合。
	 */
	public int remainder(int leftOpe, int rightOpe) throws ArithmeticException {
		return leftOpe % rightOpe;
	}

	/**
	 * 符号を反転させる。
	 *
	 * @param operand オペランド
	 * @return 符号を反転させた値
	 */
	public double invertSign(double operand) {
		return -operand;
	}

	/**
	 * インクリメントする。
	 *
	 * @param operand オペランド
	 * @return インクリメントさせた値
	 */
	public int increase(int operand) {
		// 後置インクリメント
		//return operand++;

		// 前置インクリメント
		return ++operand;
	}

	/**
	 * デクリメントする。
	 *
	 * @param operand オペランド
	 * @return デクリメントさせた値
	 */
	public int decrease(int operand) {
		// 後置デクリメント
		//return operand--;

		// 前置デクリメント
		return --operand;
	}

	/**
	 * 前置インクリメントの結果を文字列で返す。
	 *
	 * @param operand オペランド
	 * @return 前置インクリメントの結果
	 */
	public String printPreIncrement(int operand) {
		String result = "";

		result += "++operand = " + ++operand;
		result += "operand = " + operand;

		return result;
	}

	/**
	 * 後置インクリメントの結果を文字列で返す。
	 *
	 * @param operand オペランド
	 * @return 前置インクリメントの結果
	 */
	public String printPostIncrement(int operand) {
		String result = "";

		result += "operand++ = " + operand++;
		result += "operand = " + operand;

		return result;
	}
}
