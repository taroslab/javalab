package operator;

/**
 * 論理演算子
 *
 * @author Taro
 */
public class LogicalOperator {

	/**
	 * 論理積(AND)。
	 * 値が範囲内かどうか判定する。
	 *
	 * @param value 値
	 * @return 値が範囲内の場合はtrue、範囲内の場合はfalse
	 */
	public boolean isInRange(int value) {
		return 0 <= value && value <= 10;
	}

	/**
	 * 論理和(OR)。
	 * 値が範囲外かどうか判定する。
	 *
	 * @param value 値
	 * @return 値が範囲外の場合はtrue、範囲内の場合はfalse
	 */
	public boolean isOutOfRange(int value) {
		return value < 0 || 10 < value;
	}

	/**
	 * 否定(NOT)。
	 * 真偽を反転する。
	 *
	 * @param condition 真偽の状態
	 * @return 真偽を反転された状態
	 */
	public boolean change(boolean condition) {
		return !condition;
	}
}
