package operator;

/**
 * 関係演算子
 *
 * @author Taro
 */
public class RelationalOperator {

	/**
	 * 左オペランドと右オペランドが等しいかどうか判定する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 左オペランドと右オペランドが等しい場合はtrue、等しくない場合はfalse
	 */
	public boolean isEqual(int leftOpe, int rightOpe) {
		return leftOpe == rightOpe;
	}

	/**
	 * 左オペランドと右オペランドが等しくないかどうか判定する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 左オペランドと右オペランドが等しくない場合はtrue、等しい場合はfalse
	 */
	public boolean isNotEqual(int leftOpe, int rightOpe) {
		return leftOpe != rightOpe;
	}

	/**
	 * 左オペランドが右オペランドより大きいかどうか判定する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 左オペランドが右オペランドより大きい場合はtrue、等しくない場合はfalse
	 */
	public boolean isGreaterThan(int leftOpe, int rightOpe) {
		return leftOpe > rightOpe;
	}

	/**
	 * 左オペランドが右オペランド以上かどうか判定する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 左オペランドが右オペランド以上の場合はtrue、等しくない場合はfalse
	 */
	public boolean isGreaterThanOrEqual(int leftOpe, int rightOpe) {
		return leftOpe >= rightOpe;
	}

	/**
	 * 左オペランドが右オペランドより小さいかどうか判定する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 左オペランドが右オペランドより小さい場合はtrue、等しくない場合はfalse
	 */
	public boolean isLessThan(int leftOpe, int rightOpe) {
		return leftOpe < rightOpe;
	}

	/**
	 * 左オペランドが右オペランド以下かどうか判定する。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 左オペランドが右オペランド以下の場合はtrue、等しくない場合はfalse
	 */
	public boolean isLessThanOrEqual(int leftOpe, int rightOpe) {
		return leftOpe <= rightOpe;
	}

	/**
	 * オペランドがクラス（またはそのサブクラス）と等しいかどうか判定する。
	 *
	 * @param operand オペランド
	 * @return オペランドがクラス（またはそのサブクラス）と等しい場合はtrue、等しくない場合はfalse
	 */
	public boolean isInstanceOf(Object operand) {
		return operand instanceof Integer;
	}

	// TODO: double型どうしの等価比較(安全な比較)
	// TODO: String型どうしの比較(equalをつかう)
	// TODO: オブジェクトの等価比較（参照の比較になる）
}
