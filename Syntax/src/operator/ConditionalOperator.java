package operator;

/**
 * 条件演算子
 *
 * @author Taro
 */
public class ConditionalOperator {

	/**
	 * 三項演算子。
	 * 右オペランドと左オペランドが等しいかどうか判定し、その旨の文言を返す。
	 *
	 * @param leftOpe 左オペランド
	 * @param rightOpe 右オペランド
	 * @return 右オペランドと左オペランドが等しい場合は"Equal"、等しくない場合は"Not equal"を返す。
	 */
	public String judge(int leftOpe, int rightOpe) {
		return leftOpe == rightOpe ? "Equal" : "Not equal";
	}
}
