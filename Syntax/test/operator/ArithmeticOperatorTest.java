package operator;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

/**
 * 算術演算子テスト
 *
 * @author Taro
 */
class ArithmeticOperatorTest {

	private ArithmeticOperator target = new ArithmeticOperator();

	/**
	 * 加算のテスト
	 * a + b
	 */
	@Test
	void testAdd() {
		assertThat(target.add(1.0, 2.0), is(3.0));
	}

	/**
	 * 減算のテスト
	 * a - b
	 */
	@Test
	void testSubtract() {
		assertThat(target.subtract(1.0, 2.0), is(-1.0));
	}

	/**
	 * 乗算のテスト
	 * a * b
	 */
	@Test
	void testMultiply() {
		assertThat(target.multiply(2.0, 3.0), is(6.0));
	}

	/**
	 * 除算のテスト
	 * a / b
	 */
	@Test
	void testDivide() {
		assertThat(target.divide(10.2, 5.1), is(2.0));
	}

	/**
	 * 整数の除算のテスト
	 * (int)a / (int)b
	 */
	@Test
	void testDivideInt() {
		assertThat(target.divide(10, 3), is(3));
	}

	/**
	 * 剰余算のテスト
	 * a % b
	 */
	@Test
	void testRemainder() {
		assertThat(target.remainder(10, 3), is(1));
	}

	/**
	 * 符号反転のテスト
	 * -a
	 */
	@Test
	void testInvertSign() {
		assertThat(target.invertSign(-1.0), is(1.0));
	}

	/**
	 * インクリメントのテスト
	 * ++a
	 */
	@Test
	void testIncrease() {
		assertThat(target.increase(1), is(2));
	}

	/**
	 * デクリメントのテスト
	 * --a
	 */
	@Test
	void testDecrease() {
		assertThat(target.decrease(1), is(0));
	}

	/**
	 * 前置インクリメントのテスト
	 * ++a
	 */
	@Test
	void testPreIncrement() {
		String expected = "++operand = 2" + "operand = 2";
		assertThat(target.printPreIncrement(1), is(expected));
	}

	/**
	 * 後置インクリメントのテスト
	 * a++
	 */
	@Test
	void testPostIncrement() {
		String expected = "operand++ = 1" + "operand = 2";
		assertThat(target.printPostIncrement(1), is(expected));
	}
}
